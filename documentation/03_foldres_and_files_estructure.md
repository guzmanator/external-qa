# 3) analizar lo que ya tenemos

## porque se analiza lo que tenemos

Elegir una semilla es un camino facil, pero nop todas son iguales, si no conocemos en profundidad como funciona y que nos da, estaremos dando palos de ciego.

No todas las cosas de una semilla tienen que estar bien hechas, lo han desarrollado personas, y puede haber un monton de cosas que necesitaras adaptar.

Aun con todo el tiempo que dedicaras es solo una parte de lo que gastarias en investigar todas las posibilidades y escribirlo todo tu.

## entendiendo la estructura de directorios 

Lo primero es ver como se distribuye todo y que es cada cosa para entender como tienes que trabajar, la estructura que veras es algo como 

 []build
 []documentation
 []node_modules
 []src
 []test
 []tools
 .DS_Store
 .editorconfig
 .flowconfig
 .gitattributes
 .gitignore
 .travis.yml
 CHANGELOG.md
 CONTRIBUTING.md
 LICENSE.txt
 package.json
 README.md


Cada uno de los directorios se usa para:
[]build: este directorio solo aparece despues de construir, es lo que se sube al servidor, minificado y demas 

[]node_modules: es el directorio donde se ponen los paquetes de las dependencias que usa el proyecto 

[]src: es el directorio donde esta el codigo fuente de la aplicacion

[]test: es el directorio con los test de la aplicacion 

[]tools: es el directorio donde estan los js de la app que se usan para contruirla, levantar el servidor, copiar ficheros...


Los ficheros que hay a nivel raiz se usan para:
.DS_Store: fichero de mac que guarda cosas temporales, se puede eliminar

.editorconfig: es el fichero donde pones las cosas que el editor de texto de tu equipo de trabajo usara para que todos trabajen de la misma manera, por ejemplo espacios en vez de tabs http://editorconfig.org/

.flowconfig: es una version del config.ini pero mas moderna

.gitattributes: fichero de configuracion de git 

.gitignore: fichero de configuracion de git con la lista de ficheros que no hay que subir 

.travis.yml: fichero de configuracion de travis (es un sistema de Integracion continua)

CHANGELOG.md: lista de cambios del proyecto, borro el contenido apra poner el mio 

CONTRIBUTING.m: lista de gente que colabora en el proyecto 

LICENSE.txt: tipo de licencia que tiene el proyecto que creamos, MIT es la base de la aplicacion, nuestra version no tiene por que ser la misma licencia 

package.json: fichero que trae las dependencias de nuestro proyecto 

README.md: fichero que explica como se construye y usa el proyecto.


## Que hay dentro del directorio build  

Este directorio solo aparece despues de construir, es lo que se sube al servidor, minificado y demas.

La estructura que veras dentro es algo como:
[]public
     []css
     []fonts
     apple-touch-icon.png
     browserconfig.xml
     crossdomain.xml
     favicon.ico
     humans.txt
     logo.png
     robots.txt
     tile-wide.png
     tile.png
assets.js: fichero que indica cual es el main de la app 
package.json: dependencias 
server.js: js de la aplicacion, incluye la aprte server y la vista
server.js.map: para encontrar las cosas en el minificado

En el directorio public se sube la parte estatica de la aplicacion:
[]public

     []css: estilos de la aplicacion 
     []fonts: fuentes que se utilizan (ademas de font awesome)
     apple-touch-icon.png: un icono para mostrar en dispositivos apple
     browserconfig.xml: lo mismo que el anterior pero para microsoft
     crossdomain.xml: fichero de politicas para acceder a otros dominos
     favicon.ico: icono que aparece en la pestaña de la aplicacion en el navegador 
     humans.txt: fichero para indicar quien trabajo en el proyecto 
     logo.png: logo de la app 
     robots.txt: fichero para los robots de google
     tile-wide.png: una imagen de fondo
     tile.png: una imagen de fondo


## Que hay dentro del directorio src
Este es el directorio donde esta el codigo fuente de la aplicacion, la estructura principal es  
[]components
[]content
[]core
[]data
[]public
[]routes
[]vendor
 client.js
 config.js
 server.js


[]components: aqui es donde tenemos creados compoenntes de react, para la aplicacion, la cabecera, los widgets...


[]content: contenido a mostrar en modo markdown

[]core: js que se utilizan como core de la app, tenemos una implementacion del logueo con passport, cosas para el dom y un polyfill para las peticiones ajax

[]data: directorio con modelos, querys y tipos para leer datos, en este caso hay un ejemplo de leer feeds de reactjsnews

[]public: contenido base que ira en a carpeta public

[]routes: cada una de las rutas a las que se puede navegar desde el menu de la izquierda, aqui estan los contenidos de las paginas que se muestran 

[]vendor: directorio donde ponemos los js de terceros, en este caso tenemos la libreria de graficos que usamos

 client.js: el js principal de la app

 config.js: configuracion y claves de acceso a servicios 

 server.js: el js de express que sirve la app 

 ## Que hay dentro del directorio tools
es el directorio donde estan los js de la app que se usan para contruirla, levantar el servidor, copiar ficheros...

La estructura que tiene es:

 [] lib
.eslintrc
build.js
bundle.js
clean.js
copy.js
deploy.js
README.md
render.js
run.js
runServer.js
start.js
webpack.config.js



 [] lib directorio para js de librerias como leer ficheros o hacer peticiones http

.eslintrc: configuracion del lintado para los ficheros 

build.js: configuaracion de como se construye la aplicacion 

bundle.js: fichero de webpack para generar el bundle

clean.js: fichero de configuracion para borrar el directorio de build y empezar la cosntrucion de limpio

copy.js: fichero para copiar al directorio de build desde los originales 

deploy.js: fichero para hacer un despliegue 

README.md: documentacion de como esta montado 

render.js: configuracion de como tratar las rutas

run.js: fichero par configurar la ejecucion del server

runServer.js: fichero par ejecutar el server

start.js: fichero para inciar el proceso de webpack de start cuando queieres ejecutar la app en local 

webpack.config.js: configuracion base de webpack

