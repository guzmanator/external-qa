#1) Definiendo nuestra aplicacion

## Descripcion de la aplicacion 

Queremos hacer una aplicacion que permita a los desarrolladores, qa, Product manager y consultores de negocio, ganar dinero dando a las empresas ideas, bugs y mejoras.

## Que problema queremos solucionar 

Las empresas gastan mucho dinero en contratar gente a tiempo completo, o a consultoras que les hagan informes muy caros, empresas como Niatic no tienen suficiente gente como para evolucionar su producto al ritmo que marca el mercado.

Las empresas gastan mucho tiempo y dinero contratando gente que hasta unos meses de trabajo realmente no saben si les van a aportar, ademas encontrar el talento resulta muy complejo.

Hay gente muy buena y muy preparada pero que nunca llegarian a los procesos de seleccion, ademas hay gente a la que le gustaria ganar un extra pero sin entrar a trabajar directamente en las empresas.

Las empresas gastan mucho dinero en procesos para entender a sus clientes y como mejorar, pero las personas a las que preguntan no tienen los conocimientos como para generar aportaciones de alta calidad.

## Como lo solucionamos 

Ofrecemos una plataforma donde la gente puede ofrecer una descripcion detallada de un bug, una mejora o una idea para una empresa, como si trabajasen para ella y fuese a ser entregada directamente ael equipo de desarrolladores.

tenemos dos tipos de clientes, los que contactan con notros y nos piden nuestros servicios y los que no son clientes pero los usuarios les ofrecen su trabajo.

## Que existe en el mercado 

Para proyectos opensource existen los pull request, pero las empresas no tienen su codigo abierto.
-
```javascript
    https://blog.newrelic.com/2014/05/05/open-source_gettingstarted
```

Tambien existen lugares donde la gente paga por que se implemente una feature
-
```javascript
    https://www.bountysource.com
```

Tambien existen empresas que ofrecen recompensas por encontrar bug generalmente de seguridad
-
```javascript
    https://bugcrowd.com/list-of-bug-bounty-programs
```

## Como empezar

Lo primero es elegir un set de tecnologias, como realizare mi idea para startup mientras hago la formacion de react, el set de tecnologias que emplearemos seran 

- React
- redux
- rxjs
- express
- mongo 

Lo siguiente es "nunca empieces de cero", si pretendes hacerlo todo tu a mano es como empezar una casa cociendo ladrillos o escabando el metal en una mina para luego fundirlo y hacer las varas de metal para el forjado.

He elegido un theme basico para tener un algo incial 
-
```javascript
    https://github.com/start-react/sb-admin-react
```

Este theme es famoso por ser sencillo de usar y tener implementaciones en puro css, en angular y en react.+

No me interesa que sea bonito, me interesa un time to market lo as reducido posible.