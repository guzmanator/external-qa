# 3) creando una pagina estatica nueva 

clonar una pagina que ya existe 
-
```javascript
external-qa/src/routes/dashboardPages/blank
```

cambiarle el nombre a la carpeta 
-
```javascript
bugs
```

cambiarle el nombre al blank.js
-
```javascript
bugs.js
```

editar el index.js para que en lugar de cargar blank cargue bugs
-
```javascript
import React from 'react';
import Blank from './bugs';

export default {
  path: '/bugs',

  action() {
    return <bugs />;
  },

};
```

cambiar el contenido de bugs.js para que sea la nueva pagina
-
```javascript 
	import React, { PropTypes } from 'react';
	import { PageHeader } from 'react-bootstrap';

	const title = 'Bugs';

	function displayBugs(props, context) {

	  return (
	    <div>
	      <div className="row">
	        <div className="col-lg-12">
	          <PageHeader>Bugs</PageHeader>
	        </div>
	      </div>
	    </div>
	  );
	}


	displayBugs.contextTypes = { setTitle: PropTypes.func.isRequired };
	export default displayBugs;
```


editar el src/routes/index.js

importamos el nuevo componente
-
```javascript 
   import bugs from './dashboardPages/bugs';
```

en la lista de rutas ponemos la nueva pagina
-
```javascript 
    children: [
    ....
     blank,
     bugs,
```

en el menu añadimos el nuevo enlace /src/components/Sidebar/index.js
http://fontawesome.io/icon/bug/
-
```javascript 
            <li>
              <a href="" onClick={(e) => { e.preventDefault(); history.push('/'); }} >
                <i className="fa fa-dashboard fa-fw" /> &nbsp;Dashboard
              </a>
            </li>
           <li>
              <a href="" onClick={(e) => { e.preventDefault(); history.push('/bugs'); }} >
                <i className="fa fa-bug fa-fw" /> &nbsp;Bugs
              </a>
            </li>
```

para personalizar la pagina podemos ir a /src/routes/home/home.js
y copiar el import 
nota: tener en ccuenta que la ruta relativa es un ../ de mas 
-
```javascript 
import StatWidget from '../../../components/Widget';
```

pegar el contenido de mas 
-
```javascript 

     <div className="row">
        <div className="col-lg-3 col-md-6">
          <StatWidget
            style="panel-primary"
            icon="fa fa-comments fa-5x"
            count="26"
            headerText="New Comments!"
            footerText="View Details"
            linkTo="/"
          />
        </div>
        <div className="col-lg-3 col-md-6">
          <StatWidget
            style="panel-green"
            icon="fa fa-tasks fa-5x"
            count="12"
            headerText="New Tasks!"
            footerText="View Details"
            linkTo="/"
          />
        </div>
        <div className="col-lg-3 col-md-6">
          <StatWidget
            style="panel-yellow"
            icon="fa fa-shopping-cart fa-5x"
            count="124"
            headerText="New Orders!"
            footerText="View Details"
            linkTo="/"
          />
        </div>
        <div className="col-lg-3 col-md-6">
          <StatWidget
            style="panel-red"
            icon="fa fa-support fa-5x"
            count="13"
            headerText="Support Tickets!"
            footerText="View Details"
            linkTo="/"
          />
        </div>
      </div>
```