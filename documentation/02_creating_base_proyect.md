# 2) creando la base del proyecto 

## manos en el codigo, tener un repositorio con el codigo

Me creo un direcotrio de proyectos o me muevo a el si ya lo tengo 
-
```javascript
    mkdir proyectos 
    cd proyectos
```

Clono el repositorio 
-
```javascript
    git clone https://github.com/start-react/sb-admin-react.git
```

Me voy al directorio generado
-
```javascript
    cd .\sb-admin-react\
```

Instalo las dependencias con npm 
-
```javascript
    npm install
```

verifico que funciona, debe de abrirse un navegador
-
```javascript
    npm start
```

el repositorio esta linkado al original 
-
```javascript
   git remote show origin
```

creo un repositorio nuevo en bitbucket 
-
```javascript
      *ir a bitbucket 

      *repository / create a repository 

      *darle nombre cells-ngob-simple-demo

      *desmarcar privado si aplica 

      *pulsar boton crear 
```

cambiar la vinculacion al nuestro 
-
```javascript
   git remote set-url origin  https://guzmanator@bitbucket.org/guzmanator/external-qa.git
```

Subir el codigo 
-
```javascript
   git push -u origin master
```

Modificamos el bower json para que tenga una version y nuestro nombre de proyecto
-
```javascript
   "version": "0.0.1",
   "name": "external-qa",
```

subimos a git la version inicial del proyecto
-
```javascript
    git add .
    git commit -m "version 0.0.1"
    git push origin master
    git tag 0.0.1
    git push origin 0.0.1
```
