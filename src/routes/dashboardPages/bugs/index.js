import React from 'react';
import Bugs from './bugs';

export default {
  path: '/bugs',

  action() {
    return <Bugs />;
  },

};